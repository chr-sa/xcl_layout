# xcl_layout

My personal keyboard layout, optimized for german and english (50/50).
The layout was optimized with the [AdNW-optimizer](http://www.adnw.de/).
The download can be found [here](http://www.adnw.de/index.php?n=Main.Downloads).

The optimizer was compiled with:
```sh
g++ -std=c++11 -Wall -O2 -DNDEBUG -DMIT_THREADS -DOHNE2SHIFT opt.cc -o opt
```
The layout was generated with:
```sh
./opt -G 6 -2 deutsch.txt -G 2 -3 deutsch.txt -G 6 -2 englisch.txt -G 2 -3 englisch.txt -K xcl.cfg
```
The effort grind was inspired by the [Colemak DH effort grid](https://colemakmods.github.io/mod-dh/model.html).

The presented layout was not the optimum, but was very close score wise.
Further, the p/d and ,/u where swapped, to account for my preference for curling the index fingers instead of extending them.
This swap did not change the score significantly.

The layout was also compared to other layouts using following analyzers:
- [stevep99's Keyboard Layout Analyzer](https://stevep99.github.io/keyboard-layout-analyzer/#/main)
- [Colemak-DH Analyzer](https://colemakmods.github.io/mod-dh/analyze.html)
- [bclnr's kb-layout-evaluation](https://github.com/bclnr/kb-layout-evaluation)


## The Layout

```
x c l p v ü , o . q ß
s r n t g y i e a h b
f w m d j ö u ä z k
```

## Implementations

This layout is implemented in kmonad and qmk. Both implementations can be found in this repository.

# Mod 30

This is an adaptation to the layout intended for 3x5 keyboards.

```
x c l p b / , o . f
s r n t g y i e a h
v w m d j ' u q z k
```

German Umlaute are intended to be accessed through combos:

```
ü = , + o
ö = u + q
ä = q + z
ß = . + f
``` 

## Derivation

This layout was drived with the following optimizer:

```sh
g++ -std=c++11 -O2 -DNDEBUG -DTASTENZAHL=33 -DMIT_THREADS opt.cc -o opt30
```

Optimized was with the command

```sh
./opt30 -G 6 -2 englisch.txt -G 2 -3 englisch.txt -G 6 -2 deutsch.txt -G 2 -3 deutsch.txt -K xcl_mod30.cfg
```

The layout was not the optimal one found, but scorewise pretty close.
Some letters (p/b and c/l) were swapped manually to better resemble the xcl layout, which I am already decently comfortable with.
I am not sure about the c/l swap myself, one would have to decide what to use oneself.
The swaps and the layout was further analyzed with multiple layouts, just like the main layout.

The configuration `xcl_mod30.cfg` was used.
The effort grid was taken from [bclnr's kb-layout-evaluation](https://github.com/bclnr/kb-layout-evaluation), as it aligns with my own preferences pretty well.
